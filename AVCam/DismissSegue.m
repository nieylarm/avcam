//
//  DismissSegue.m
//  AVCam
//
//  Created by mjunaidi on 1/6/15.
//  Copyright (c) 2015 Apple Inc. All rights reserved.
//

#import "DismissSegue.h"

@implementation DismissSegue

- (void)perform {
    UIViewController *sourceViewController = self.sourceViewController;
    [sourceViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end