//
//  AVCamAppDelegate.h
//  AVCam
//
//  Created by mjunaidi on 1/6/15.
//  Copyright (c) 2015 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AVCamAppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic) UIWindow *window;

@end
