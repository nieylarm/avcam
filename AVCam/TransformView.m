//
//  TransformView.m
//  AVCam
//
//  Created by mjunaidi on 1/7/15.
//  Copyright (c) 2015 Apple Inc. All rights reserved.
//

#import "TransformView.h"

@implementation TransformView

+ (Class)layerClass {
    return [CATransformLayer class];
}

@end