//
//  ViewController.m
//  AVCam
//
//  Created by mjunaidi on 1/6/15.
//  Copyright (c) 2015 BT. All rights reserved.
//

#import "AddViewController.h"

@interface AddViewController () {
    // Define keys
    NSString *nameKey;
    NSString *descriptionKey;
    NSString *latitudeKey;
    NSString *longitudeKey;
}
@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    nameKey = @"name";
    descriptionKey = @"description";
    latitudeKey = @"latitude";
    longitudeKey = @"longitude";
    
    self.descriptionFld.layer.borderWidth = 1.0f;
    self.descriptionFld.layer.borderColor = [[UIColor grayColor] CGColor];
    
    [self initLocationManager];
    
    [self.saveBtn setTarget:self];
    [self.saveBtn setAction:@selector(save)];
}

- (NSNumber*) parseNumber:(NSString*) str {
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber * val = [f numberFromString:str];
    return val;
}

- (void)save {
    NSLog(@"Saving...");
    
    NSString* name = self.nameFld.text;
    NSString* description = self.descriptionFld.text;
    NSString* lat = self.latitudeFld.text;
    NSString* lng = self.longitudeFld.text;
    
    NSNumber* latitude = [self parseNumber:lat];
    NSNumber* longitude = [self parseNumber:lng];
    
    NSLog(@"%@", name);
    NSLog(@"%@", description);
    NSLog(@"%@", latitude);
    NSLog(@"%@", longitude);
    
    NSDictionary *locationDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                name, nameKey,
                                description, descriptionKey,
                                latitude, latitudeKey,
                                longitude,longitudeKey,
                                nil];
    NSError* error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:locationDict
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString* jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", jsonStr);
    
    NSString* urlStr = @"http://192.168.1.6:8080/location/api/add";
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:jsonData];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initLocationManager {
    //start location manager
    lm = [[CLLocationManager alloc] init];
    lm.delegate = self;
    lm.desiredAccuracy = kCLLocationAccuracyBest;
    lm.distanceFilter = kCLDistanceFilterNone;
    [lm startUpdatingLocation];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 &&
        ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse
        || [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways)
        ) {
        // Will open an confirm dialog to get user's approval
        [lm requestWhenInUseAuthorization];
        //[lm requestAlwaysAuthorization];
    } else {
        [lm startUpdatingLocation]; //Will update location immediately
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {

    CLLocation * location = [locations lastObject];
    
    //get latest location coordinates
    CLLocationDegrees latitude = location.coordinate.latitude;
    CLLocationDegrees longitude = location.coordinate.longitude;
    
    NSString* txt = [NSString stringWithFormat:@"%+.2f, %+.2f", latitude, longitude];
    
    NSLog(@"%@", txt);
    
    // set to input fields
    NSString* lat = [[NSNumber numberWithDouble:latitude] stringValue];
    NSString* lng = [[NSNumber numberWithDouble:longitude] stringValue];
    
    [self.latitudeFld setText:lat];
    [self.longitudeFld setText:lng];
    
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            NSLog(@"User still thinking..");
        } break;
        case kCLAuthorizationStatusDenied: {
            NSLog(@"User hates you");
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            NSLog(@"updating");
            [lm startUpdatingLocation]; //Will update location immediately
        } break;
        default:
            break;
    }
}

@end
