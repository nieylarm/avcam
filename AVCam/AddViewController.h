//
//  ViewController.h
//  AVCam
//
//  Created by mjunaidi on 1/6/15.
//  Copyright (c) 2015 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>

@interface AddViewController : UIViewController<CLLocationManagerDelegate, UITextFieldDelegate> {
    CLLocationManager *lm;
}

@property (strong, nonatomic) IBOutlet UITextField *nameFld;
@property (strong, nonatomic) IBOutlet UITextView *descriptionFld;
@property (strong, nonatomic) IBOutlet UITextField *latitudeFld;
@property (strong, nonatomic) IBOutlet UITextField *longitudeFld;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveBtn;

@end

