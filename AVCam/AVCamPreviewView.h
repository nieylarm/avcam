//
//  AVCamPreviewView.h
//  AVCam
//
//  Created by mjunaidi on 1/6/15.
//  Copyright (c) 2015 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AVCaptureSession;

@interface AVCamPreviewView : UIView

@property (nonatomic) AVCaptureSession *session;

@end
