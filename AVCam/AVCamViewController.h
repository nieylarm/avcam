//
//  AVCamViewController.h
//  AVCam
//
//  Created by mjunaidi on 1/6/15.
//  Copyright (c) 2015 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "ViewController.h"
#import "TransformView.h"

@interface AVCamViewController : UIViewController <CLLocationManagerDelegate, ModalViewControllerDelegate> {
    
    CLLocationManager *lm; //core lcoation manager instance
    
}

@property (weak, nonatomic) IBOutlet TransformView *transformView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowView;

@end
