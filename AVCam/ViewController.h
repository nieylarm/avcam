//
//  ViewController.h
//  AVCam
//
//  Created by mjunaidi on 1/6/15.
//  Copyright (c) 2015 BT. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ModalViewControllerDelegate <NSObject>
- (void)addItemViewController:(id)controller latitude:(NSNumber*)latitude longitude:(NSNumber*)longitude;
@end

@interface ViewController : UIViewController<UITableViewDelegate>
{
    IBOutlet UITableView *tableData;
}

@property (nonatomic, assign) id <ModalViewControllerDelegate> delegate;

@end
