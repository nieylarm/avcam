//
//  ViewController.m
//  AVCam
//
//  Created by mjunaidi on 1/6/15.
//  Copyright (c) 2015 BT. All rights reserved.
//

#import "ViewController.h"
#import "AVCamViewController.h"

@interface ViewController ()
{
    NSMutableArray *myObject;
    // A dictionary object
    NSDictionary *dictionary;
    // Define keys
    NSString *nameKey;
    NSString *descriptionKey;
    NSString *latitudeKey;
    NSString *longitudeKey;
}
@end

@implementation ViewController

@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    nameKey = @"name";
    descriptionKey = @"description";
    latitudeKey = @"latitude";
    longitudeKey = @"longitude";
    
    myObject = [[NSMutableArray alloc] init];
    
    NSString* urlStr = @"http://192.168.1.6:8080/location/api/test";
    //NSString* urlStr = @"http://192.168.20.112:8080/location/api/test";
    
    NSData *jsonSource = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlStr]];
    
    id jsonObjects = [NSJSONSerialization JSONObjectWithData:
                      jsonSource options:NSJSONReadingMutableContainers error:nil];
    
    for (NSDictionary *dataDict in jsonObjects) {
        NSString *name = [dataDict objectForKey:nameKey];
        NSString *description = [dataDict objectForKey:descriptionKey];
        NSNumber *latitude = [dataDict objectForKey:latitudeKey];
        NSNumber *longitude = [dataDict objectForKey:longitudeKey];
        
        NSLog(@"NAME: %@",name);
        NSLog(@"LAT: %@",latitude);
        NSLog(@"LNG: %@",longitude);
        
        dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                      name, nameKey,
                      description, descriptionKey,
                      latitude, latitudeKey,
                      longitude,longitudeKey,
                      nil];
        [myObject addObject:dictionary];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return myObject.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Item";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell=[[UITableViewCell alloc]initWithStyle:
              UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    NSDictionary *tmpDict = [myObject objectAtIndex:indexPath.row];
    
    NSMutableString *text;
    text = [NSMutableString stringWithFormat:@"%@", [tmpDict objectForKeyedSubscript:nameKey]];
    
    NSMutableString *detail;
    detail = [NSMutableString stringWithFormat:@"%@ ", [tmpDict objectForKey:descriptionKey]];
    
    
    cell.textLabel.text = text;
    cell.detailTextLabel.text= detail;
    
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *tmpDict = [myObject objectAtIndex:indexPath.row];
    
    NSNumber* latitude = [tmpDict objectForKey:latitudeKey];
    NSNumber* longitude = [tmpDict objectForKey:longitudeKey];
    
    NSLog(@"LAT: %@",latitude);
    NSLog(@"LNG: %@",longitude);
    
    NSString* msg = [NSMutableString stringWithFormat:@"%@, %@", latitude, longitude];
    
    UIAlertView *messageAlert = [[UIAlertView alloc]
                                 initWithTitle:@"Row Selected" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // Display Alert Message
    //[messageAlert show];
    
    
    [self.delegate addItemViewController:self latitude:latitude longitude:longitude];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
